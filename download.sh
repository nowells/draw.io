#!/bin/bash

COOKIES='cookie: <COPIED FROM CONFLUENCE>'

curl 'https://confluence.nvidia.com/rest/drawio/1.0/config?_=1706124335529' -q \
  -H 'accept: application/json, text/javascript, */*; q=0.01' \
  -H "$COOKIES" |
  jq -r .uiConfig |
  sed -e "s/\/download\/attachments\/2395626920\/NVIDIA%20Sans%20/https:\/\/brand-assets.cne.ngc.nvidia.com\/assets\/fonts\/nvidia-sans\/1.0.0\/NVIDIASans_/g" |
  sed -e "s/?api=v2//g" |
  sed -e "s/woff'/woff2'/g" |
  sed -e "s/woff)/woff2)/g" |
  sed -e "s/_Regular/_Rg/g" |
  sed -e "s/_Italic/_It/g" |
  sed -e "s/_Medium/_Md/g" |
  sed -e "s/_Light/_Lt/g" |
  sed -e "s/_Bold/_Bd/g" |
  jq . |
  tee drawio.config.json

echo '
{
  "language": "",
  "configVersion": "1.3",
  "customFonts": [],
  "libraries": "general;uml;er;bpmn;flowchart;basic;arrows2",
  "customLibraries": [
    "L.scratchpad"
  ],
  "plugins": [],
  "recentColors": [],
  "formatWidth": "240",
  "createTarget": false,
  "pageFormat": {
    "x": 0,
    "y": 0,
    "width": 850,
    "height": 1100
  },
  "search": true,
  "showStartScreen": true,
  "gridColor": "#d0d0d0",
  "darkGridColor": "#424242",
  "autosave": true,
  "resizeImages": null,
  "openCounter": 1,
  "version": 18,
  "unit": 1,
  "isRulerOn": false,
  "ui": ""
}
' >drawio.prefs.json

cat drawio.config.json | jq -r '.defaultCustomLibraries | map(. | sub("A";"")) | join("\n")' | while read -r LIBRARY; do
  DATA="$(curl -q -H "$COOKIES" "https://confluence.nvidia.com/rest/drawio/1.0/library/${LIBRARY}")"
  NAME="$(echo "$DATA" | jq .name | sed -e 's/[^A-Za-z0-9._ -]/-/g' | sed -e 's/^-*//g' | sed -e 's/-*$//g' | sed -e 's/\.xml$//g')"
  echo "$DATA" | jq -r .xml | tee "libraries/${NAME}.xml"
  cat <<<"$(jq ".customLibraries += [\"Uhttps%3A%2F%2Fgitlab.com%2Fnowells%2Fdraw.io%2F-%2Fraw%2Fmain%2Flibraries%2F$(echo "$NAME" | sed "s/ /%20/g").xml\"]" drawio.prefs.json)" >drawio.prefs.json
done

cat <<<"$(jq ".defaultCustomLibraries = []" drawio.config.json)" >drawio.config.json

CONFIG="$(cat drawio.config.json)"
PREFS="$(cat drawio.prefs.json)"
CODE="$(echo "
(function() {
if (window.location.hostname != \"app.diagrams.net\") {
  alert(\"Unable to setup draw.io, you must be on https://app.diagrams.net/\");
} else {
  localStorage.setItem(\".drawio-config\", JSON.stringify($PREFS));
  localStorage.setItem(\".configuration\", JSON.stringify($CONFIG));
  window.location.reload();
}
})()
" | jq -sRr @uri)"
echo "<h1>NVIDIA Draw.IO Bookmarklet</h1><a href=\"javascript:$CODE\">Setup NVIDIA Draw.io</a>" >index.html
